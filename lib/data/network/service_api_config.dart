
import 'dart:convert';

import 'package:http/http.dart' show Client, Response;
import 'package:tes_flutter_get_x/data/network/response/response_message.dart';
import 'package:tes_flutter_get_x/utils/constant.dart';


/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class ServiceApiConfig {
  Client client = Client();
  String base_url = BASE_URL;

  Future<ResponseMessage> postForm(
      String name, String email, String no,
      String dob, String age, String gender,
      String country, String password
      ) async {
    Response response;
    response = await client.post("$base_url/input_form.php",
        body: {
          'name': name, 'email': email, 'no': no,
          'dob': dob, 'age': age, 'gender': gender,
          'country': country, 'password': password
        });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseMessage.fromJson(json.decode(response.body));
    } else {
      print('$base_url');
      throw Exception('${response.body}');
    }
  }

}