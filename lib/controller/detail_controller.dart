import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tes_flutter_get_x/data/network/service_api_config.dart';
import 'package:tes_flutter_get_x/ui/page/detail_page.dart';
import 'package:tes_flutter_get_x/ui/widget/widget_utils.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class DetailController extends GetxController {

  @override
  void onInit() {
    print('chiy');
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void toast(BuildContext context, String text) {
    showToast(context, text);
  }

  String formatDate(String date){
    String format= date.replaceAll("00:00:00.000", "");
    print(format);
    return format;
  }

}
