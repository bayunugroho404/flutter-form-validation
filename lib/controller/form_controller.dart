import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:tes_flutter_get_x/data/network/service_api_config.dart';
import 'package:tes_flutter_get_x/ui/page/detail_page.dart';
import 'package:tes_flutter_get_x/ui/widget/widget_utils.dart';
import 'package:url_launcher/url_launcher.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class FormController extends GetxController {
  @override
  void onInit() {
    super.onInit();
  }

  void postLogin(
      BuildContext context,
      String name,
      String email,
      String no,
      String dob,
      String age,
      String gender,
      String country,
      String password,
      Map<String, dynamic> value,
      ProgressDialog pr) async {
    ServiceApiConfig()
        .postForm(name, email, no, dob, age, gender, country, password)
        .then((data) {
      if (data.message == "Data Inserted Successfully") {
        pr.hide();
        customDialog(context, "Success", "${data.message}", () {
          update();
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => DetailPage(
                  value['name'],
                  value['email'],
                  value['mobile'],
                  '${value['date']}',
                  value['gender'],
                  value['age'],
                  value['country'],
                  value['password']),
            ),
          );
        });
      } else {
        pr.hide();
        showToast(context, data.message);
      }
    }).catchError((e) {
      pr.hide();
      showToast(context, "$e");
      print('server ada kendala,silahkan coba kemblai ');
    });
  }

  @override
  void onClose() {
    super.onClose();
  }

  void toast(BuildContext context, String text) {
    showToast(context, text);
  }

  void report(BuildContext context) {
    reportDialog(context, "Report Bug", "Submit", () {
      launchUrl("https://api.whatsapp.com/send?phone=+6282125990090");
    });
  }

  void launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
