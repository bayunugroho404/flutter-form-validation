import 'package:flutter/material.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

Widget MyTextField({String title,String value ,bool obscureText =false}){
  return  Padding(
    padding: const EdgeInsets.all(20.0),
    child: TextFormField(
      enabled: false,
      obscureText: obscureText,
      initialValue: value,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: title,
        hintText: 'Enter Name Here',
      ),
      autofocus: false,

    ),
  );
}