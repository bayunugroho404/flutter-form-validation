import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tes_flutter_get_x/ui/page/form_page.dart';
import 'package:toast/toast.dart';

import '../../main.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

void showToast(BuildContext context, String msg, {int duration, int gravity}) {
  Toast.show(msg, context, duration: Toast.LENGTH_SHORT);
}

void showSnackBar(String title, String meesage) {
  Get.snackbar(
    "$title",
    "$meesage",
    snackPosition: SnackPosition.BOTTOM,
  );
}

// ignore: non_constant_identifier_names
void customDialog(
    BuildContext context, String title, String desk, Function onPress) {
  AwesomeDialog(
    context: context,
    dialogType: DialogType.SUCCES,
    animType: AnimType.BOTTOMSLIDE,
    title: '$title',
    desc: '$desk',
    btnCancelOnPress: () {},
    btnOkOnPress: onPress,
  )..show();
}

void reportDialog(
    BuildContext context, String title, String desk, Function onPress) {
  AwesomeDialog(
    context: context,
    dialogType: DialogType.ERROR,
    animType: AnimType.BOTTOMSLIDE,
    title: '$title',
    desc: '$desk',
    btnCancelOnPress: () {},
    btnOkOnPress: onPress,
  )..show();
}

void showDialog(String title, String content) {
  Get.defaultDialog(
    title: "$title",
    content: Text("$content"),
  );
}

final pages = [
  Container(
    color: Colors.white,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Bayu Nugroho",
                style: MyApp.goldcoinGreyStyle,
              ),
              InkWell(
                onTap: () {
                  Get.off(MyFormPage());
                },
                child: Text(
                  "Skip",
                  style: MyApp.goldcoinGreyStyle,
                ),
              ),
            ],
          ),
        ),
        Image.asset("assets/img3.png"),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Love",
                style: MyApp.greyStyle,
              ),
              Text(
                "Flutter",
                style: MyApp.boldStyle,
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
                style: MyApp.descriptionGreyStyle,
              ),
            ],
          ),
        )
      ],
    ),
  ),
  Container(
    color: Color(0xFF55006c),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Bayu Nugroho",
                style: MyApp.goldCoinWhiteStyle,
              ),
              InkWell(
                onTap: () {
                  Get.off(MyFormPage());
                },
                child: Text(
                  "Skip",
                  style: MyApp.goldcoinGreyStyle,
                ),
              ),
            ],
          ),
        ),
        Image.asset("assets/img4.png"),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Love",
                style: MyApp.whiteStyle,
              ),
              Text(
                "Music",
                style: MyApp.boldStyle,
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                style: MyApp.descriptionWhiteStyle,
              ),
            ],
          ),
        )
      ],
    ),
  ),
  Container(
    color: Colors.orange,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Bayu Nugroho",
                style: MyApp.goldCoinWhiteStyle,
              ),
              InkWell(
                onTap: () {
                  Get.off(MyFormPage());
                },
                child: Text(
                  "Skip",
                  style: MyApp.goldcoinGreyStyle,
                ),
              ),
            ],
          ),
        ),
        Image.asset("assets/img1.png"),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Love",
                style: MyApp.whiteStyle,
              ),
              Text(
                "Coding",
                style: MyApp.boldStyle,
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
                style: MyApp.descriptionGreyStyle,
              ),
            ],
          ),
        )
      ],
    ),
  ),
];
