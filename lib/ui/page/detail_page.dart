import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tes_flutter_get_x/controller/detail_controller.dart';
import 'package:tes_flutter_get_x/controller/form_controller.dart';
import 'package:tes_flutter_get_x/ui/widget/widget_textfield.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class DetailPage extends StatefulWidget {
  String name = "";
  String email = "";
  String no = "";
  String dob = "";
  String age = "";
  String gender = "";
  String country = "";
  String password = "";

  DetailPage(
      this.name,
      this.email,
      this.no,
      this.dob,
      this.age,
      this.gender,
      this.country,
      this.password);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {

  final DetailController _controller = Get.put(DetailController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail'),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              MyTextField(title: "Name", value: widget.name),
               MyTextField(title: "Email",value: widget.email),
               MyTextField(title: "Phone Number",value: widget.no),
               MyTextField(title: "Date Of Birt",value: _controller.formatDate(widget.dob)),
               MyTextField(title: "Age",value: widget.age),
               MyTextField(title: "Gender",value: widget.gender),
               MyTextField(title: "Country",value: widget.country),
               MyTextField(title: "Password",value: widget.password,obscureText:true),
            ],
          ),
        ),
      ),
    );
  }
}
