import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:tes_flutter_get_x/controller/form_controller.dart';
import 'package:tes_flutter_get_x/ui/widget/widget_button.dart';
import 'package:tes_flutter_get_x/ui/widget/widget_utils.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class MyFormPage extends StatefulWidget {

  @override
  MyFormPageState createState() => MyFormPageState();

}

class MyFormPageState extends State<MyFormPage> {
  ProgressDialog pr;
  final formKey = GlobalKey<FormBuilderState>();
  final FormController _formController = Get.put(FormController());

  @override
  Widget build(BuildContext context) {
    pr= new ProgressDialog(context);
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Form valdiation"),
          actions: [
            IconButton(onPressed: () {
              _formController.report(context);
            }, icon: Icon(Icons.person))
          ],
        ),
        body: FormBuilder(
            key: formKey,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(15),
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        FormBuilderTextField(
                          attribute: "name",
                          validators: [FormBuilderValidators.required()],
                          decoration: InputDecoration(
                              icon: Icon(Icons.person),
                              hintText: "Enter your name",
                              labelText: "Name"
                          ),
                        ),
                        SizedBox(height: 25),
                        FormBuilderTextField(
                          attribute: "email",
                          validators: [FormBuilderValidators.required(), FormBuilderValidators.email()],
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                              icon: Icon(Icons.email),
                              hintText: "Enter your email",
                              labelText: "Email"
                          ),
                        ),
                        SizedBox(height: 25),
                        FormBuilderTextField(
                          attribute: "mobile",
                          validators: [FormBuilderValidators.required(), FormBuilderValidators.numeric()],
                          decoration: InputDecoration(
                              icon: Icon(Icons.phone),
                              hintText: "Enter your mobile no",
                              labelText: "Mobile No"
                          ),
                        ),
                        SizedBox(height: 25),
                        FormBuilderDateTimePicker(
                          attribute: "date",
                          inputType: InputType.date,
                          format: DateFormat("dd-MM-yyyy"),
                          decoration: InputDecoration(labelText: "Date of Birth"),
                          validators: [FormBuilderValidators.required()],
                        ),
                        SizedBox(height: 25),
                        FormBuilderTextField(
                          attribute: "age",
                          decoration: InputDecoration(labelText: "Age"),
                          keyboardType: TextInputType.number,
                          maxLength: 3,
                          validators: [
                            FormBuilderValidators.numeric(),
                            FormBuilderValidators.max(70),
                          ],
                        ),
                        SizedBox(height: 25),
                        FormBuilderRadioGroup(
                          attribute: "gender",
                          decoration: InputDecoration(
                              labelText: "Gender",
                              labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 22)
                          ),
                          options: [
                            FormBuilderFieldOption(value: "Male"),
                            FormBuilderFieldOption(value: "Female"),
                            FormBuilderFieldOption(value: "Custom"),
                          ],
                        ),
                        SizedBox(height: 25),
                        // ignore: deprecated_member_use
                        FormBuilderCheckboxList(
                          attribute: "languages",
                          initialValue: ["English"],
                          decoration: InputDecoration(
                              labelText: "Languages You Speak",
                              labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 22)
                          ),
                          options: [
                            FormBuilderFieldOption(value: "English"),
                            FormBuilderFieldOption(value: "Spanish"),
                            FormBuilderFieldOption(value: "Indoensia"),
                          ],
                        ),
                        SizedBox(height: 25),
                        FormBuilderCountryPicker(
                          attribute: "country",
                          initialValue: "United States",
                          decoration: InputDecoration(
                              labelText: "Country",
                              labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 22)
                          ),
                        ),
                        SizedBox(height: 25),
                        FormBuilderTextField(
                          attribute: "password",
                          decoration: InputDecoration(labelText: "Password"),
                          obscureText: true,
                          maxLines: 1,
                          maxLength: 16,
                          validators: [
                            FormBuilderValidators.required(),
                            FormBuilderValidators.minLength(8),
                            FormBuilderValidators.maxLength(16)],
                        ),
                        SizedBox(height: 40),
                        ButtonSubmit(onPress: (){
                          formKey.currentState.save();
                          if(formKey.currentState.validate()){
                            pr.show();
                            _formController.postLogin(context,
                                formKey.currentState.value['name'],
                                formKey.currentState.value['email'],
                                formKey.currentState.value['mobile'],"${formKey.currentState.value['date']}",
                                formKey.currentState.value['age'], formKey.currentState.value['gender'],
                                formKey.currentState.value['country'], formKey.currentState.value['password'],formKey.currentState.value,pr);
                          }else{
                            _formController.toast(context, "Periksa Data Kembali");
                          }
                        }),
                        SizedBox(height: 30),
                      ],
                    ),
                  ),
                ),
              ),
            )
        ),
      ),
    );
  }
}
